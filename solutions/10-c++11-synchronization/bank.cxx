#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <random>
#include <thread>
#include <mutex>
#include <tuple>

using namespace std;

struct Account {
    recursive_mutex lock;
    int             balance = 0;
};

tuple<Account*, Account*>
pick_two(vector<Account>& accounts, default_random_engine& r) {
    const auto                         lb      = 0U;
    const auto                         ub      = static_cast<const unsigned>(accounts.size() - 1);
    uniform_int_distribution<unsigned> nextAccount{lb, ub};
    auto                               fromIdx = nextAccount(r);
    auto                               toIdx   = 0U;
    do {
        toIdx = nextAccount(r);
    } while (fromIdx != toIdx);
    return make_tuple(&accounts[fromIdx], &accounts[toIdx]);
}


void Updater(int id, vector<Account>* accounts, int N, int T) {
    cout << ("Updater-" + to_string(id) + " started\n");
    default_random_engine r;

    for (int k = 0; k < N; ++k) {
        auto[from, to] = pick_two(*accounts, r);

        lock(from->lock, to->lock);
        lock_guard<recursive_mutex> l1(from->lock, adopt_lock);
        lock_guard<recursive_mutex> l2(to->lock, adopt_lock);

        from->balance -= T;
        to->balance += T;
    }

    cout << ("Updater-" + to_string(id) + " done\n");
}

int main(int numArgs, char* args[]) {
    auto numUpdaters     = 10UL;
    auto numTransactions = 10'000UL;
    auto numAccounts     = numUpdaters / 2;
    auto amount          = 100;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-u") numUpdaters = stoul(args[++k]);
        else if (arg == "-t") numTransactions = stoul(args[++k]);
        else if (arg == "-a") numAccounts = stoul(args[++k]);
        else {
            cerr << "usage: " << args[0] << " [-a num accounts] [-u <num updaters>] [-t <num transactions>]\n";
            return 1;
        }
    }

    cout << "A = " << numAccounts << endl;
    cout << "U = " << numUpdaters << endl;
    cout << "N = " << numTransactions << endl;
    cout << "T = " << amount << endl;

    vector<Account> accounts{numAccounts};
    vector<thread>  updaters;
    updaters.reserve(numUpdaters);

    for (auto k = 0UL; k < numUpdaters; ++k) {
        updaters.emplace_back(&Updater, k + 1, &accounts, numTransactions, amount);
    }
    for (auto& t : updaters) t.join();

    cout << "--------------\n";
    cout << "SUM balance = "
         << accumulate(accounts.begin(), accounts.end(), 0,
                       [](int sum, Account& a) { return sum + a.balance; })
         << endl;

    return 0;
}
