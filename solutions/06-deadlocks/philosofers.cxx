#include <iostream>
#include <vector>
#include "ChopStick.hxx"
#include "Philosofer.hxx"

using namespace std;
using namespace cppThreads;

void swap(ChopStick*& cs1, ChopStick*& cs2);

int main(int numArgs, char* args[]) {
    auto numPhilosofers = 5U;
    auto noDeadlock     = false;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-p") {
            numPhilosofers = static_cast<unsigned int>(stoi(args[++k]));
        } else if (arg == "-no") {
            noDeadlock = true;
        } else {
            cerr << "usage: " << args[0] << " [-p <num philos>] [-no]" << endl;
            return 1;
        }
    }
    cout << "# philosofers: " << numPhilosofers << endl;
    cout << "NO deadlock  : " << boolalpha << noDeadlock << endl;

    vector<ChopStick>   chopsticks(numPhilosofers);
    vector<Philosofer*> philosofers;

    for (unsigned k = 0; k < chopsticks.size(); ++k) {
        ChopStick* left  = &chopsticks[k % chopsticks.size()];
        ChopStick* right = &chopsticks[(k + 1) % chopsticks.size()];

        if (noDeadlock && k == 0) swap(left, right);

        philosofers.push_back(new Philosofer(k, left, right));
    }
    philosofers[0]->join();

    return 0;
}


void swap(ChopStick*& cs1, ChopStick*& cs2) {
    ChopStick* cs = cs1;
    cs1 = cs2;
    cs2 = cs;
}
