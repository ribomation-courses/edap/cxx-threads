#pragma once

#include "thrlib/Mutex.hxx"
#include "thrlib/Condition.hxx"
#include "thrlib/Guard.hxx"

using namespace cppThreads;

class ChopStick {
    bool      busy = false;
    Mutex     mutex;
    Condition notBusy;

public:
    ChopStick() : notBusy(mutex) { }

    void acquire() {
        Guard g(mutex);
        notBusy.waitUntil([&]() { return !busy; });
        busy = true;
    }

    void release() {
        Guard g(mutex);
        busy = false;
        notBusy.notifyAll();
    }
};


