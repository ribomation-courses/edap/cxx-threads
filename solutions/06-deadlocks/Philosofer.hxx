#pragma  once

#include <iostream>
#include <sstream>
#include <string>
#include <random>
#include <unistd.h>
#include "thrlib/Thread.hxx"
#include "ChopStick.hxx"


using namespace std;
using namespace std::string_literals;
using namespace cppThreads;


class Philosofer : public Thread {
    ChopStick* left;
    ChopStick* right;
    const string prefix;

public:
    Philosofer(unsigned id, ChopStick* left, ChopStick* right)
            : left(left), right(right),
              prefix{string(id * 15, ' ') + "P-"s + to_string(id + 1) + ":"s} {
        start();
    }

protected:
    void run() {
        const int                          MS = 1000;
        uniform_int_distribution<unsigned> nextSleepTime(100 * MS, 300 * MS);
        default_random_engine              r;

        while (true) {
            log("THINKING");
            //usleep(nextSleepTime(r));

            log("WAIT LEFT");
            left->acquire();
            log("WAIT RIGHT");
            right->acquire();

            log("EATING");
            usleep(nextSleepTime(r));

            right->release();
            left->release();
        }
    }

    void log(const char* msg) {
        cout << (prefix + msg + "\n"s);
    }
};
