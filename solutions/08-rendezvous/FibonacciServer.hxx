#pragma once

#include <iostream>
#include <sstream>
#include "thrlib/Rendezvous.hxx"

using namespace std;
using namespace cppThreads;


class FibonacciServer : public RendezvousThread<unsigned, unsigned long> {
public:
    FibonacciServer() { start(); }

protected:
    void run() override {
        for (;;) {
            auto msg = recv();
            auto n = msg->getPayload();
            if (n == 0) {
                msg->reply(0);
                return;
            }
            log(n);
            auto result = fib(n);
            msg->reply(result);
        }
    }

private:
    unsigned long fib(unsigned n) {
        return n <= 2 ? 1 : fib(n - 1) + fib(n - 2);
    }

    void log(unsigned n) {
        ostringstream buf;
        buf << "[server] n=" << n << "\n";
        cout << buf.str();
    }
};


