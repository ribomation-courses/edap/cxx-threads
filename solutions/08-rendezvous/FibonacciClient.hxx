#pragma  once

#include <iostream>
#include <sstream>
#include <random>
#include "FibonacciServer.hxx"

using namespace std;
using namespace std::string_literals;
using namespace cppThreads;


class Client : public Thread {
    unsigned       id;
    unsigned       numArgs;
    FibonacciServer& srv;

public:
    Client(unsigned id, unsigned numArgs, FibonacciServer& srv)
            : id(id), numArgs(numArgs), srv(srv) {
        start();
    }

protected:
    void run() override {
        default_random_engine              r;
        uniform_int_distribution<unsigned> nextArg{id, 42};

        for (unsigned k = 1; k <= numArgs; ++k) {
            auto arg = nextArg(r);
            log(k, arg);

            auto result = srv.send(arg);
            log(k, arg, result);
        }
        cout << ("Client-"s + to_string(id) + "] done\n"s);
    }

private:
    void log(unsigned k, unsigned arg) {
        ostringstream buf;
        buf << "[client-" << id << "] <" << k << "> fib(" << arg << ") ...\n";
        cout << buf.str();
    }

    void log(unsigned k, unsigned arg, unsigned long result) {
        ostringstream buf;
        buf << "[client-" << id << "] <" << k << "> fib(" << arg << ") = " << result << "\n";
        cout << buf.str();
    }
};
