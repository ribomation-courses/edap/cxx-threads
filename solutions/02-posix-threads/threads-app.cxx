#include <iostream>
#include <vector>
#include "Hello.hxx"

using namespace std;
using namespace cppThreads;


int main(int numArgs, char* args[]) {
    auto numThreads  = 10U;
    auto numMessages = 10'000U;

    for (int k        = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-m") {
            numMessages = static_cast<unsigned>(stoi(args[++k]));
        } else if (arg == "-t") {
            numThreads = static_cast<unsigned>(stoi(args[++k]));
        }
    }

    vector<Hello*> threads;
    for (unsigned  id = 1; id <= numThreads; ++id) {
        threads.push_back(new Hello(id, numMessages));
    }
    for (auto      t : threads) t->join();
    for (auto      t : threads) delete t;

    return 0;
}
