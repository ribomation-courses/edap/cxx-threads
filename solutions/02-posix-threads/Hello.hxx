
#include <iostream>
#include <sstream>
#include "Thread.hxx"

class Hello : public cppThreads::Thread {
    const unsigned id;
    const unsigned N;

    std::string makeName(unsigned id) {
        std::ostringstream buf;
        buf << std::string((id - 1) * 4, ' ') << "[" << id << "] ";
        return buf.str();
    }

public:
    Hello(unsigned id, unsigned N) : id{id}, N{N} {
        start();
    }

protected:
    void run() override {
        auto name = makeName(id);
        std::cout << (name + "started\n");

        for (unsigned k = 1; k <= N; ++k) std::cout << (name + "\n");

        std::cout << (name + "done\n");
    }
};

