#include <iostream>
#include <string>

#include "thrlib/Thread.hxx"
#include "mqlib/Receivable.hxx"
#include "mqlib/MessageThread.hxx"

using namespace std;
using namespace cppThreads;

using XXL = unsigned long long;


class Producer : public Thread {
    int N;
    Receivable<XXL>* next;

public:
    Producer(int N, Receivable<XXL>* next) : N{N}, next{next} {
        start();
    }

protected:
    void run() override {
        for (int k = 1; k <= N; ++k) { next->send(k); }
        next->send(0);
    }
};

class Transformer : public MessageThread<XXL> {
    Receivable<XXL>* next;

public:
    Transformer(Receivable<XXL>* next) : next{next} { start(); }

protected:
    void run() override {
        for (auto n = recv(); n != 0; n = recv()) {
            next->send(2 * n);
        }
        next->send(0);
    }
};


class Consumer : public MessageThread<XXL> {
public:
    Consumer() { start(); }

protected:
    void run() override {
        for (auto n = recv(); n != 0; n = recv()) {
            cout << "[consumer] " << n << endl;
        }
    }
};


int main(int numArgs, char* args[]) {
    int numTransformers = 3;
    int numMessages     = 10;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numTransformers = stoi(args[++k]);
        } else if (arg == "-m") {
            numMessages = stoi(args[++k]);
        } else {
            cerr << "usage: " << args[0] << " [-t <num trans>] [-m <num msgs>]\n";
            return 1;
        }
    }

    Consumer c;
    Receivable<XXL>* next = &c;
    while (numTransformers-- > 0) { next = new Transformer{next}; }
    Producer p{numMessages, next};

    c.join();

    return 0;
}

