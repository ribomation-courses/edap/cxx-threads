#include <iostream>
#include "thrlib/Thread.hxx"
#include "mqlib/MessageQueue.hxx"

using namespace std;
using namespace cppThreads;


class Consumer : public Thread {
    MessageQueue<int>& in;

public:
    explicit Consumer(MessageQueue<int>& in) : in(in) {
        start();
    }

protected:
    void run() override {
        for (int n = 0; (n = in.get()) > 0;) {
            cout << "[consumer] " << n << endl;
        }
    }
};


int main(int numArgs, char* args[]) {
    auto N = (numArgs > 1) ? stoi(args[1]) : 42;

    MessageQueue<int> q{4};
    Consumer          c{q};

    for (int k = 1; k <= N; ++k) q.put(k);
    q.put(-1);
    c.join();

    return 0;
}

