//compile: g++ -std=c++14 -Wall -fmax-errors=1 -pthread HelloThreads.cpp -o hello-threads
//run    : ./hello-threads -t <int> -m <int>

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <thread>
#include <cstdlib>

using namespace std;
using namespace std::string_literals;

int main(int numArgs, char *args[]) {
    unsigned numThreads = 25;
    unsigned numMessages = 10'000;

    for (int k = 1; k < numArgs; ++k) {
        string arg = args[k];
        if (arg == "-t") {
            numThreads = static_cast<unsigned int>(stoi(args[++k]));
        } else if (arg == "-m") {
            numMessages = static_cast<unsigned int>(stoi(args[++k]));
        } else {
            cerr << "usage: " << args[0] << " [-t <int>] [-m <int>]" << endl;
            ::exit(1);
        }
    }

    auto run = [=](unsigned id) {
        for (auto k = 0U; k < numMessages; ++k) {
            ostringstream buf;
            buf << string(id * 4, ' ') << id << "\n";
            cout << buf.str();
            //cout << ("..."s + to_string(id) + "\n"s);
        }
    };

    vector<thread> threads;
    for (auto k = 0U; k < numThreads; ++k)
        threads.emplace_back(run, k + 1);
    for (auto &t : threads) t.join();

    return 0;
}
