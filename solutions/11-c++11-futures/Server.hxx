#pragma once

#include <iostream>
#include <future>
#include "Message.hxx"
#include "MessageQueue.hxx"

using namespace std;

class Server {
    MessageQueue<Message<int, long>*> inbox;
    string                            name;
    function<long(int x)>             compute;

public:
    Server(string n, function<long(int x)> f) : name{n}, compute{f} {}

    future<long> request(Message<int, long>* m) {
        inbox.put(m);
        return m->reply.get_future();
    }

    void run() {
        cout << (name + "started\n");
        bool running = true;
        try {
            do {
                Message<int, long>* m = inbox.get();
                cout << (name + "recv: " + to_string(m->message) + "\n") << flush;

                long result = compute(m->message);

                cout << (name + "rply: " + to_string(m->message)
                         + " -> " + to_string(result) + "\n") << flush;
                m->reply.set_value(result);

                running = (m->message >= 0);
            } while (running);
        } catch (...) { cerr << "WTF !!" << endl; }
        cout << (name + "done\n");
    }
};


