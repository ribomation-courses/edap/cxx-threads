cmake_minimum_required(VERSION 3.8)
project(12_C__11_Futures)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "-Wall -Wextra -Werror -Wfatal-errors -pthread")

add_executable(three-servers
        Message.hxx
        MessageQueue.hxx
        Server.hxx
        three-servers.cxx
        )

