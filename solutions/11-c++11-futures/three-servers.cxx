#include <iostream>
#include <string>
#include <functional>
#include <thread>
#include <future>
#include "Message.hxx"
#include "MessageQueue.hxx"
#include "Server.hxx"

using namespace std;
using namespace std::string_literals;


long _fib(int n) { return n <= 2 ? 1 : _fib(n - 1) + _fib(n - 2); }
long _fac(int n) { return n <= 1 ? 1 : n * _fac(n - 1); }
long _sum(int n) { return n <= 1 ? 1 : n + _sum(n - 1); }


int main(int numArgs, char* args[]) {
    int numMessages = 100;

    for (int i = 1; i < numArgs; ++i) {
        string arg = args[i];
        if (arg == "-m") numMessages = stoi(args[++i]);
    }
    cout << "numMessages: " << numMessages << endl;

    Server fib("[fibonacci] ", [](int x) { return _fib(x); });
    Server fac("[factorial] ", [](int x) { return _fac(x); });
    Server sum("[sum] ", [](int x) { return _sum(x); });
    
    thread fibThr(&Server::run, &fib);
    thread facThr(&Server::run, &fac);
    thread sumThr(&Server::run, &sum);

    thread client([&] {
        cout << ("[client] started\n");
        for (int i = 1; i <= numMessages; ++i) {
            auto* m1 = new Message<int, long>{i % 45};
            auto* m2 = new Message<int, long>{i % 10};
            auto* m3 = new Message<int, long>{i % 100};

            future<long> f1 = fib.request(m1);
            future<long> f2 = fac.request(m2);
            future<long> f3 = sum.request(m3);

            cout << ("[client] waiting for replies (" + to_string(i) + ")...\n") << flush;
            f1.wait();
            f2.wait();
            f3.wait();

            cout << "[client] fib(" << m1->message << ") = " << f1.get() << endl;
            cout << "[client] fac(" << m2->message << ") = " << f2.get() << endl;
            cout << "[client] sum(" << m3->message << ") = " << f3.get() << endl;

            delete m3;
            delete m2;
            delete m1;
        }

        cout << "[client] shutting down the servers...\n" << flush;
        {
            auto f1 = fib.request(new Message<int, long>{-1});
            auto f2 = fac.request(new Message<int, long>{-1});
            auto f3 = sum.request(new Message<int, long>{-1});
            f1.wait_for(chrono::milliseconds(10));
            f2.wait_for(chrono::milliseconds(10));
            f3.wait_for(chrono::milliseconds(10));
        }
        cout << "[client] done\n" << flush;
    });

    client.join();
    fibThr.join();
    facThr.join();
    sumThr.join();

    return 0;
}
