#include <iostream>
#include <string>
#include <vector>
#include <pthread.h>
#include <algorithm>

using namespace std;
using namespace std::string_literals;

unsigned numMessages = 1000;

void* run(void* arg) {
    auto         id  = (unsigned long) (arg);
    string       tab(id * 5, ' ');
    const string msg = tab + "["s + to_string(id) + "]\n"s;

    for (auto k = 0U; k < numMessages; ++k) { cout << msg; }
    return nullptr;
}

int main(int argc, char** argv) {
    unsigned numThreads = 5;

    for (auto k = 1; k < argc; ++k) {
        string arg = argv[k];
        if (arg == "-m") {
            numMessages = static_cast<unsigned>(stoi(argv[++k]));
        } else if (arg == "-t") {
            numThreads = static_cast<unsigned>(stoi(argv[++k]));
        }
    }

    vector<pthread_t> threads;

    for (auto k = 0UL; k < numThreads; ++k) {
        pthread_t tid;
        pthread_create(&tid, nullptr, &run, (void*) (k + 1));
        threads.push_back(tid);
    };

    for_each(threads.begin(), threads.end(), [&](pthread_t& tid) {
        pthread_join(tid, nullptr);
    });

    return 0;
}
