#pragma once

#include <functional>
#include <pthread.h>
#include "Mutex.hxx"

namespace cppThreads {
    class Condition {
        Mutex& mutex;
        pthread_cond_t condition;

    public:
        Condition(Mutex& mutex) : mutex(mutex) {
            pthread_cond_init(&condition, NULL);
        }

        ~Condition() {
            pthread_cond_destroy(&condition);
        }

        Condition() = delete;
        Condition(const Condition&) = delete;

        void wait() {
            pthread_cond_wait(&condition, mutex.native());
        }

        void wait(std::function<bool()> ready) {
            while (!ready()) wait();
        }

        void notifyOne() {
            pthread_cond_signal(&condition);
        }

        void notifyAll() {
            pthread_cond_broadcast(&condition);
        }
    };

    void guarded(Mutex& m, Condition& c, std::function<bool()> until, std::function<void()> stmts) {
        Guard g(m);
        c.wait([&]() { return until(); });
        stmts();
        c.notifyAll();
    }

}
