Demo applications
====

Unpack
----

    cd path/to/my/stuff
    tar xvfz path/to/demo/ATM.tar.gz
    tar xvfz path/to/demo/SWIFT.tar.gz

Build and Execute
----

Load an unpacked project into CLion and build and run it from there
or create a build dir and use cmake. Here is how to do it for the ATM demo.

    cd path/to/ATM
    mkdir build
    cd build
    cmake ..
    make
    ./bank-unsafe
    ./bank-safe

